import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Permission, PermissionDomain } from '../../models/permission';
import { PermissionService } from '../../services/permission/permission.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public constructor(
    private readonly router: Router,
    private readonly permissionService: PermissionService,
    ) {}
  public canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.permissionService.hasPermission$([[PermissionDomain.ManageStudents, Permission.Read]]).pipe(
      map(hasPermission => {
        if (!hasPermission) {
          this.router.createUrlTree(['/']);
        }
        return hasPermission;
      })
    )
  }
  
}
