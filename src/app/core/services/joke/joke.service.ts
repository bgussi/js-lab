import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JokeService {
  constructor(
    private readonly http: HttpClient
  ) {}

  public getRandomJoke$(): Observable<string> {
    return this.http.get('https://api.chucknorris.io/jokes/random?category=dev')
      .pipe(
        pluck('value')
      );
  }

  public getErrRandomJoke$(): Observable<string> {
    return this.http.get('https://api.chucknorris.io/jokes/asdasdasdasdas')
      .pipe(
        pluck('value')
      );
  }
}
