import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { PermissionDomain } from '../../models/permission';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {
  public constructor(
    private readonly userService: UserService
  ) {}

  public hasPermission$(neededPermissionPairs: [PermissionDomain, number][]) {
    return this.userService.getUser$()
      .pipe(
        map(({permissions}) => {
          for(const neededPermissionPair of neededPermissionPairs) {
            const userPermission = permissions[neededPermissionPair[0]];
            const neededPermission = neededPermissionPair[1];
            if (!((userPermission & neededPermission) === neededPermission)) {
              return false;
            }
          }
          return true;
        }),
      )
  }
}
