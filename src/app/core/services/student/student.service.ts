import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { Gender } from 'src/app/shared/modules/student/models/gender';
import { School } from 'src/app/shared/modules/student/models/school';
import { Student } from 'src/app/shared/modules/student/models/student';
import { SCHOOL_TOKEN } from '../../tokens/school.token';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  public store$ = new BehaviorSubject<Student[]>([
    {
      id: 1,
      firstName: 'Yelyzaveta',
      lastName: 'Tsapyk',
      gender: Gender.GIRL,
      curriculum: [],
    },
    {
      id: 2,
      firstName: 'Yurii',
      lastName: 'Yaremko',
      gender: Gender.BOY,
      curriculum: [],
    },
    {
      id: 3,
      firstName: 'Sofiia',
      lastName: 'Kalamuniak',
      gender: Gender.GIRL,
      curriculum: [],
    },
    {
      id: 4,
      firstName: 'Sofiia',
      lastName: 'Popadiuk',
      gender: Gender.GIRL,
      curriculum: [],
    },
    {
      id: 5,
      firstName: 'Alex',
      lastName: 'Smith',
      gender: Gender.BOY,
      curriculum: [],
    },
    {
      id: 6,
      firstName: 'John',
      lastName: 'Green',
      gender: Gender.BOY,
      curriculum: [],
    },
    {
      id: 7,
      firstName: 'John',
      lastName: 'Doe',
      gender: Gender.BOY,
      curriculum: [],
    },
    {
      id: 8,
      firstName: 'Jane',
      lastName: 'Doe',
      gender: Gender.GIRL,
      curriculum: [],
    },
    {
      id: 9,
      firstName: 'Karl',
      lastName: 'Markus',
      gender: Gender.BOY,
      curriculum: [],
    },
  ]);
  constructor(
    @Inject(SCHOOL_TOKEN) private school: School,
  ) {}
  public getStudents$(): Observable<Student[]> {
    return this.store$.pipe(
      map(students => students.map(student => ({
        ...student,
        school: this.school,
      }))),
      delay(500)
    );
  }

  public getStudent$(id: number): Observable<Student> {
    return this.store$.pipe(
      map(students => students.find(student => student.id === id)!)
    )
  }

  public addStudent(student: Student) {
    const store = this.store$.value;
    student = {...student, id: store.length, curriculum: []};
    this.store$.next([
      ...store,
      student
    ]);
  }

  public editStudent(student: Student) {
    const store = [...this.store$.value];
    const oldStudentIndex = store.findIndex(s => s.id === student.id);
    store[oldStudentIndex] = student;
    this.store$.next(store);
  }
}
