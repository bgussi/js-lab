import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Permission } from '../../models/permission';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly user$ = new BehaviorSubject<User>({
    permissions: {
      ManageStudents: Permission.Read + Permission.Write + Permission.Delete, 
    },
  });

  public getUser$() {
    return this.user$.asObservable();
  }
}
