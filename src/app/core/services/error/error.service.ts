import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(private readonly _snackBar: MatSnackBar) {}

  public handleError$(err: any) {
    this._snackBar.open(err.error.message, undefined, );
    return throwError(err);
  }
}
