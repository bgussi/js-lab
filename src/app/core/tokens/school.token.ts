import { InjectionToken } from '@angular/core';
import { School } from 'src/app/shared/modules/student/models/school';

export const SCHOOL_TOKEN = new InjectionToken<School>('School');