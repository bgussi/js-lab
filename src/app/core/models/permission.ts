export enum Permission {
    Read = 4,
    Write = 2,
    Delete = 1,
}

export enum PermissionDomain {
    ManageStudents = 'ManageStudents',
}