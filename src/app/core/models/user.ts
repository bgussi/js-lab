import { PermissionDomain } from './permission';

export interface User {
    permissions: {
        [key in PermissionDomain]: number
    }
}