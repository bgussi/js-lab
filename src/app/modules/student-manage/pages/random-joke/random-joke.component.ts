import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { JokeService } from 'src/app/core/services/joke/joke.service';

@Component({
  selector: 'app-random-joke',
  templateUrl: './random-joke.component.html',
  styleUrls: ['./random-joke.component.scss']
})
export class RandomJokeComponent implements OnInit {
  public joke$!: Observable<string>;
  constructor(
    private readonly jokeService: JokeService
  ) { }

  public ngOnInit(): void {
    this.joke$ = this.jokeService.getRandomJoke$();
  }

}
