import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { StudentService } from 'src/app/core/services/student/student.service';
import { Gender } from 'src/app/shared/modules/student/models/gender';

@Component({
  selector: 'app-student-new',
  templateUrl: './student-new.component.html',
  styleUrls: ['./student-new.component.scss']
})
export class StudentNewComponent implements OnInit {
  @ViewChild(FormGroupDirective) private readonly formDirective!: FormGroupDirective;
  public studentForm!: FormGroup;
  public gender = Gender;
  public title = 'Add Student Form';
  public button = 'Add Student';

  constructor(
    protected readonly formBuilder: FormBuilder,
    protected readonly studentService: StudentService,
  ) {}

  public ngOnInit(): void {
    this.studentForm = this.formBuilder.group({
      id: this.formBuilder.control(''),
      firstName: this.formBuilder.control('', [Validators.required, Validators.minLength(2)]),
      lastName: this.formBuilder.control('', [Validators.required, Validators.minLength(2)]),
      gender: this.formBuilder.control(this.gender.BOY, [Validators.required]),
    });
  }

  public submit() {
    this.studentService.addStudent(this.studentForm.value);
    this.formDirective.resetForm();
  }

}
