import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { StudentService } from 'src/app/core/services/student/student.service';
import { StudentNewComponent } from '../student-new/student-new.component';

@Component({
  selector: 'app-student-edit',
  templateUrl: '../student-new/student-new.component.html',
  styleUrls: ['../student-new/student-new.component.scss']
})
export class StudentEditComponent extends StudentNewComponent implements OnInit {
  public title = 'Update Student Form';
  public button = 'Update Student';
  constructor(
    protected readonly formBuilder: FormBuilder,
    protected readonly studentService: StudentService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly matSnackBar: MatSnackBar,
  ) {
    super(formBuilder, studentService);
  }

  public ngOnInit(): void {
    console.log(this.activatedRoute.snapshot);
    const student = this.activatedRoute.snapshot.data.student;
    this.studentForm = this.formBuilder.group({
      id: this.formBuilder.control(student?.id),
      firstName: this.formBuilder.control(student?.firstName, [Validators.required, Validators.minLength(2)]),
      lastName: this.formBuilder.control(student?.lastName, [Validators.required, Validators.minLength(2)]),
      gender: this.formBuilder.control(student?.gender || this.gender.BOY, [Validators.required]),
    });
  }

  public submit() {
    this.studentService.editStudent(this.studentForm.value);
    this.matSnackBar.open(`Successfully updated ${this.studentForm.value.firstName}`);
  }

}
