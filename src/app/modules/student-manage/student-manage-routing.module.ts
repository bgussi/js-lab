import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RandomJokeComponent } from './pages/random-joke/random-joke.component';
import { StudentEditComponent } from './pages/student-edit/student-edit.component';
import { StudentNewComponent } from './pages/student-new/student-new.component';
import { StudentsResolver } from './resolvers/students.resolver';
import { StudentManageComponent } from './student-manage.component';

const routes: Routes = [
  {
    path: '',
    component: StudentManageComponent,
  },
  { path: 'new', component: StudentNewComponent},
  { path: 'random-joke', component: RandomJokeComponent},
  { path: ':studentId', component: StudentEditComponent, resolve: {student: StudentsResolver}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentManageRoutingModule { }
