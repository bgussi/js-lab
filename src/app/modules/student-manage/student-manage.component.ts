import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { StudentService } from 'src/app/core/services/student/student.service';
import { Student } from 'src/app/shared/modules/student/models/student';

@Component({
  selector: 'app-student-manage',
  templateUrl: './student-manage.component.html',
  styleUrls: ['./student-manage.component.scss']
})
export class StudentManageComponent implements OnInit {
  public students$!: Observable<Student[]>;
  constructor(
    private readonly studentService: StudentService,
  ) {}

  public ngOnInit() {
    this.students$ = this.studentService.getStudents$();
  }

}
