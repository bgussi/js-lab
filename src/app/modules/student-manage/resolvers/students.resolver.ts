import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { StudentService } from 'src/app/core/services/student/student.service';
import { Student } from 'src/app/shared/modules/student/models/student';

@Injectable({
  providedIn: 'root'
})
export class StudentsResolver implements Resolve<Student> {
  public constructor(
    private readonly studentService: StudentService,
  ) {}
  public resolve(route: ActivatedRouteSnapshot): Observable<Student> {
    return this.studentService.getStudent$(
      Number(route.paramMap.get('studentId')!)
    ).pipe(first());
  }
}
