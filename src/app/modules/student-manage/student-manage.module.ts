import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentManageRoutingModule } from './student-manage-routing.module';
import { StudentManageComponent } from './student-manage.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RandomJokeComponent } from './pages/random-joke/random-joke.component';
import { StudentNewComponent } from './pages/student-new/student-new.component';
import { StudentEditComponent } from './pages/student-edit/student-edit.component';


@NgModule({
  declarations: [
    StudentManageComponent,
    RandomJokeComponent,
    StudentNewComponent,
    StudentEditComponent,
  ],
  imports: [
    SharedModule,
    StudentManageRoutingModule
  ]
})
export class StudentManageModule { }
