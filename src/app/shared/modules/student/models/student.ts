import { Gender } from './gender';
import { School } from './school';
import { SchoolSubject } from './subject';

export interface Student {
    id?: number;
    firstName: string;
    lastName: string;
    gender: Gender;
    school?: School;
    curriculum?: SchoolSubject[];
}