import { Pipe, PipeTransform } from '@angular/core';
import { Gender } from '../../models/gender';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  public transform(value: Gender): string {
    return value === Gender.BOY ? '👦' : '👧';
  }

}
