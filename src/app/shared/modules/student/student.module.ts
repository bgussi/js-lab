import { Inject, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentViewComponent } from './components/student-view/student-view.component';
import { MatCardModule } from '@angular/material/card';
import {MatCommonModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import { SubjectComponent } from './components/subject/subject.component';
import { CurriculumComponent } from './components/curriculum/curriculum.component';
import { HighlightDirective } from './directive/highlight/highlight.directive';
import { GenderPipe } from './pipes/gender/gender.pipe';



@NgModule({
  declarations: [
    StudentViewComponent,
    SubjectComponent,
    CurriculumComponent,
    HighlightDirective,
    GenderPipe
  ],
  imports: [
    CommonModule,
    MatCommonModule,
    MatCardModule,
    MatButtonModule
  ],
  exports: [
    StudentViewComponent,
    GenderPipe
  ],
  providers: [],
})
export class StudentModule {
}
