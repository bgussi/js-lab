import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'validator'
})
export class ValidatorPipe implements PipeTransform {

  public transform(errors: any, label: string): string {
    if (!errors) {
      return '';
    }
    if (errors.required) {
      return `${label} is required.`;
    }
    const messages = Object.values(errors).filter(err => typeof err === 'string');
    if (messages.length === 0) {
      return `${label} is invalid.`;
    }
    return messages.join(' ');
  }

}
